//Get all todos
fetch('https://jsonplaceholder.typicode.com/todos')
.then ((response) => response.json())
.then ((data) => console.log(data))


//Get all titles only
fetch('https://jsonplaceholder.typicode.com/todos')
.then ((response) => response.json())
.then ((data) => {
	let list = data.map((todo) => {
	return todo.title;
	})
	console.log(list)
})

//Get a specific to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then ((response) => response.json())
.then ((data) => console.log(data.completed))

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then ((response) => response.json())
.then ((data) => console.log(`The item "${data.title}"" has a status of ${data.completed}.`))


//Creating a to do list item using POST method
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
	  	title: 'Created To Do List Item',
	  	completed: false,
	  	userId: 1
	})
})
.then((response) => response.json())
.then ((data) => console.log(data))


//Updating a to do list using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
	  	description: 'To update the my to do list with a different data structure.',
	  	status: 'Pending',
	  	dateCompleted: 'Pending',
	  	userId: 1
	})
})
.then((response) => response.json())
.then ((data) => console.log(data))


//Updating a to do list item using PATCH Method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: 'Complete',
	  	dateCompleted: '01/19/22'
	})
})
.then((response) => response.json())
.then ((data) => console.log(data))



//Deleting a todo list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then ((data) => console.log(data))















